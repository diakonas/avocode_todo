# API Documentation

- Feel free to use a web framework of your choice in Python
- Try to write a clean and well designed code for me to inspect
- Specify a guide to run the server app in README 
- The Server App doesn't need to have a persistent data storage, you can just store the data in-memory or whatever you prefer

## Endpoints:

### `GET /tasks`

Returns a list of tasks.

```
> GET /tasks

< 200 OK
{
  tasks: Task[] = [
    { id: number, label: string, completed: boolean, subtasks: [...]}
  ]
}
```

### `GET /tasks/:id`

Returns task by the given ID and all its sub-tasks.

```
> GET /tasks/:id

< 200 OK
{
  tasks: Task[] = [
    { id: number, label: string, completed: boolean, subtasks: [...]}
  ]
}
```

### `POST /tasks`

Creates a new task and returns it; optionally returns also nested sub-tasks, if they were specified in the request data

```
> POST /tasks
{ label: string, subtasks: [{'label': 'SubTask A'}]}

< 201 Created
{
  task: { id: number, label: string, completed: boolean, subtasks: [{'label': 'SubTask A'}]}
}
```

### `PUT /tasks/:id`

Updates the task of the given ID.  
If "completed" field is updated, all of the task's sub-tasks have this field updated as well, 
since they must be also completed when its parent is completed.

```
> PUT /tasks/:id
{ label: string } |
{ completed: boolean } |
{ label: string, completed: boolean }

< 200 OK
{
  task: Task = { id: number, label: string, completed: boolean }
}

< 404 Not Found
{ error: string }
```

### `DELETE /tasks/:id`

Deletes the task of the given ID, including all its sub-tasks

```
> DELETE /tasks/:id

< 204 No Content

< 404 Not Found
{ error: string }
```