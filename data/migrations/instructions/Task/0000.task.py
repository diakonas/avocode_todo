import yoyo



steps = [
	yoyo.step('''
		CREATE TABLE task (
			id UUID PRIMARY KEY,
			label VARCHAR NOT NULL UNIQUE,
			completed BOOLEAN DEFAULT FALSE,
			parent_id UUID NULL
		)
	''',
	'DROP TABLE task')
]
