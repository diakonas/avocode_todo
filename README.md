# Avocode TODO

## Setup
First install dependencies *(assuming virtual environment created for the project)*
```
pip install -r requirements.txt -r dev-requirements.txt
```

The app expects database connection handle and existing 'task' table in the database.  
Following two commands export database connection handle to 'DB_URI' environment variable and create the 'task' table.  

You need *yoyo-migrations* tool installed and running PostgreSQL database.  
(in this example running on *localhost* and default port *5432*)
```
export DB_URI=postgres://postgres:password@localhost:5432/todo_db
yoyo apply --no-config-file --database $DB_URI ./data/migrations/instructions/Task/
```
If you don't have PostgreSQL instance installed on your system, you can run it with *Docker* 
and export the *DB_URI* environment variable with appropriate values
```
docker run -d \
    -p 127.0.0.1:5434:5432 \
    -e POSTGRES_DB=test_db \
    -e POSTGRES_USER=test_user \
    -e POSTGRES_PASSWORD=password \
    --name postgresdb \
    postgres:10

export DB_URI=postgres://test_user:password@localhost:5434/test_db
yoyo apply --no-config-file --database $DB_URI ./data/migrations/instructions/Task/
```

## Run
```
export DB_URI=postgres://postgres:password@localhost:5432/todo_db
python -m todo
```
Or on single line
```
DB_URI=postgres://postgres:password@localhost:5432/todo_db python -m todo
```
Or via *Docker*
```
docker build -t avocode/todo:1 .
docker run --net host -e DB_URI=postgres://test_user:password@localhost:5432/test_db avocode/todo:1
```

## Make requests against the service according the API docs
[API documentation](./api-doc.md)

## Test
Run integration test locally
```
PYTHONPATH=. DB_URI=postgres://test_user:password@127.0.0.1:5434/test_db py.test todo/tests/itest_tasks.py
```
or in local GitLab runner
```
gitlab-runner exec docker integration_test
```
