from typing import Any, Callable, Dict, List, Optional, Union
import types

import asyncpg
import logging

import todo.operations



class Controller:


	def __init__(self, settings: types.ModuleType, connection: asyncpg.Connection):
		self._logger = logging.getLogger(self.__class__.__name__)
		self._settings = settings
		self._connection = connection


	async def execute_command(
		self,
		command_class: Union[todo.operations.Operation, Callable],
		payload: Dict[str, Any]
	) -> Any:
		return await command_class(self._settings, self._connection).execute(payload)


	async def query(
		self,
		query_class: Union[todo.operations.Operation, Callable],
		params: Optional[Dict[str, Any]] = None
	) -> List[Dict[str, Any]]:
		return await query_class(self._settings, self._connection).read(params)
