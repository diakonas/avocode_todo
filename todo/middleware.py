from typing import Callable, Set
import json
import uuid

import aiohttp.web
import aiohttp.web_exceptions
import asyncpg.exceptions
import logging



def error_handler() -> aiohttp.web.Response:

	logger = logging.getLogger('error-handler-middleware')


	@aiohttp.web.middleware
	async def _error_handler(request: aiohttp.web.Request, handler: Callable) -> aiohttp.web.Response:
		try:
			response = await handler(request)
		except (asyncpg.exceptions.ForeignKeyViolationError, asyncpg.exceptions.UniqueViolationError):
			logger.exception('Failed to create/update/delete a record with params = "{}"'.format(
				request.get('payload', dict(request.rel_url.query))
			))
			return aiohttp.web.HTTPConflict()
		except Exception:
			logger.exception(f'Unexpected exception has occurred, URL = "{request.url}"')
			raise
		else:
			return response

	return _error_handler



def validation_handler() -> aiohttp.web.Response:

	logger = logging.getLogger('validation-handler-middleware')


	async def _valid_payload(
		request: aiohttp.web.Request,
		required_params: Set[str],
		optional_params: Set[str],
		minimum_params: int = 0
	) -> None:
		'''
		Check if request has all required and minimum parameters for the specified route
		'''
		content = await request.content.read()
		if content:
			try:
				request['payload'].update(json.loads(content))

				if set(request['payload']) & required_params != required_params:
					raise ValueError(f'Missing parameter for request = "{request.method} {request.url}"')
				if len(set(request['payload']) & optional_params) < minimum_params:
					logger.exception(f'Too a few parameters for request = "{request.method} {request}"')
					raise ValueError(f'Too a few parameters for request = "{request.method} {request}"')
			except (KeyError, ValueError, json.JSONDecodeError) as error:
				logger.exception(f'Invalid payload for request = "{request.method} {request.url}"')
				raise aiohttp.web.HTTPBadRequest(text = f'Invalid payload, error = "{error}"')
		else:
			raise ValueError(f'Missing payload for creating task at URL = "{request.url}"')


	def _with_id(request: aiohttp.web.Request, required: bool = True) -> aiohttp.web.Request:
		try:
			request['payload']['id'] = uuid.UUID(request.match_info['id'])
		except KeyError as error:
			if required:
				logger.exception('Missing "id" of the task to delete')
				raise aiohttp.web.HTTPBadRequest(text = f'Missing "id" of the task to delete, error = "{error}"')
			else:
				pass
		else:
			return request


	@aiohttp.web.middleware
	async def _validation_handler(request: aiohttp.web.Request, handler: Callable) -> aiohttp.web.Response:
		request['payload'] = {}
		if request.method == 'GET':
			_with_id(request, required = False)
		elif request.method == 'POST':
			await _valid_payload(request, {'label'}, set())
		elif request.method == 'PUT':
			await _valid_payload(_with_id(request), set(), {'label', 'completed'}, 1)
		elif request.method == 'DELETE':
			_with_id(request, required = False)

		return await handler(request)

	return _validation_handler
