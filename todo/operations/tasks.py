from typing import Any, Dict, List, Optional
import collections
import uuid

import todo.operations
import todo.model



class CreateTasks(todo.operations.Command):

	async def execute(self, payload: Dict[str, Any]) -> todo.model.Task:
		query = f'''
			INSERT INTO task(id, label, completed, parent_id) 
			VALUES($1, $2, $3, $4) 
			RETURNING (id, label, completed, parent_id) 
		'''
		tasks = todo.model.task_list([], payload, uuid.uuid4(), None)
		await self._connection.executemany(query, tasks)
		return todo.model.task_tree(tasks)[0]  # Always only single 'top-level' task is created



class ReadTasks(todo.operations.Query):

	async def read(self, params: Dict[str, Any]) -> List[todo.model.Task]:
		if params and params.get('id'):
			query = f'''
				WITH RECURSIVE subtasks AS (
					SELECT * FROM task WHERE id = $1 
					UNION 
					SELECT 
						t.id, 
						t.label, 
						t.completed, 
						t.parent_id 
					FROM task t
					INNER JOIN subtasks s ON s.id = t.parent_id
				) SELECT * FROM subtasks ORDER BY label;
			'''
			records = [tuple(record) for record in await self._connection.fetch(query, params['id'])]
		else:
			query = 'SELECT id, label, completed, parent_id FROM task ORDER BY id;'
			records = [tuple(record) for record in await self._connection.fetch(query)]
		return todo.model.task_tree(records)



class UpdateTasks(todo.operations.Command):

	async def execute(self, payload: Dict[str, Any]) -> Optional[todo.model.Task]:
		params = collections.OrderedDict(payload)
		params.move_to_end('id', last = False)
		column_values = [f'{column} = ${index + 2}' for index, column in enumerate(list(params.keys())[1:])]
		query = f"UPDATE task SET {', '.join(column_values)} WHERE id = $1 RETURNING (id, label, completed, parent_id)"
		record = await self._connection.fetchval(query, *params.values())
		if record:
			task = todo.model.Task(*record)
			# If the "completed" parameter is to be changed, change it for all nested sub-tasks as well,
			# because "completed" parent task imply completed sub-tasks
			if 'completed' in params.keys():
				task_trees = await ReadTasks(self._settings, self._connection).read({'id': task.id})
				update_params = [
					(str(subtask.id), params['completed'])
					for subtask in todo.model.flatten_task(task_trees[0], [])
					if subtask.id != task.id
				]
				query = f"UPDATE task SET completed = $2 WHERE id = $1 RETURNING (id, label, completed, parent_id)"
				await self._connection.executemany(query, update_params)

			# I could modify the task object with its sub-tasks directly in-memory,
			# instead of making another SQL request, but I'm not focusing on optimization at this point.
			result = await ReadTasks(self._settings, self._connection).read({'id': task.id})
			return result[0]  # Return only single top-level task; the one updated
		else:
			return None



class DeleteTasks(todo.operations.Command):

	async def execute(self, params: Dict[str, Any]) -> None:
		query = f'''
			WITH RECURSIVE subtasks AS (
				SELECT id, parent_id FROM task WHERE id = $1 
				UNION 
				SELECT t.id, t.parent_id FROM task t INNER JOIN subtasks s ON s.id = t.parent_id
			) DELETE FROM task WHERE id IN (SELECT id FROM subtasks) RETURNING id;
			'''
		return await self._connection.fetchval(query, str(params['id']))
