from typing import Any, Dict
import abc
import types

import asyncpg
import logging



class Operation:


	def __init__(self, settings: types.ModuleType, connection: asyncpg.Connection) -> None:
		self._logger = logging.getLogger(self.__class__.__name__)
		self._settings = settings
		self._connection = connection



class Command(Operation):


	@abc.abstractmethod
	async def execute(self, payload: Dict[str, Any]) -> None:
		raise NotImplementedError()



class Query(Operation):


	@abc.abstractmethod
	async def read(self, params: Dict[str, Any]) -> None:
		raise NotImplementedError()
