from typing import List
import json
import types

import aiohttp.web
import logging

import todo.controller
import todo.model
import todo.operations.tasks



class API:

	TASKS = '/tasks'
	PING = '/ping'


	def __init__(
		self,
		settings: types.ModuleType,
		router: aiohttp.web.UrlDispatcher,
		controller: todo.controller.Controller
	) -> None:
		self._logger = logging.getLogger(self.__class__.__name__)
		self._settings = settings
		self._router = router
		self._controller = controller


	def register(self) -> None:
		self._router.add_routes([
			# Tasks
			aiohttp.web.post(self.TASKS, self._create_tasks),
			aiohttp.web.get(self.TASKS, self._read_tasks),
			aiohttp.web.get(f'{self.TASKS}/{{id}}', self._read_tasks),
			aiohttp.web.put(f'{self.TASKS}/{{id}}', self._update_tasks), # TODO 'POST' ?
			aiohttp.web.delete(f'{self.TASKS}/{{id}}', self._delete_tasks),
			# Health-check
			aiohttp.web.get(self.PING, self._ping),
		])


	async def _create_tasks(self, request: aiohttp.web.Request) -> aiohttp.web.Response:
		task = await self._controller.execute_command(todo.operations.tasks.CreateTasks, request['payload'])
		return aiohttp.web.json_response(
			status = aiohttp.web.HTTPCreated().status,
			data = json.dumps({'task': task.to_dict()})
		)


	async def _read_tasks(self, request: aiohttp.web.Request) -> aiohttp.web.Response:
		tasks: List[todo.model.Task] = await self._controller.query(
			todo.operations.tasks.ReadTasks,
			request.get('payload')
		)
		response = aiohttp.web.HTTPNoContent()
		if tasks:
			response = aiohttp.web.json_response(text = json.dumps(
				{'tasks': sorted([task.to_dict() for task in tasks], key = lambda task: task['label'])}
			))
		return response


	async def _update_tasks(self, request: aiohttp.web.Request) -> aiohttp.web.Response:
		task = await self._controller.execute_command(todo.operations.tasks.UpdateTasks, request['payload'])
		response = aiohttp.web.HTTPNotFound(reason = json.dumps(
			{'error': f'Task with id = "{request["payload"]["id"]}" not found'}
		))
		if task:
			response = aiohttp.web.json_response(text = json.dumps({'task': task.to_dict()}))
		return response


	async def _delete_tasks(self, request: aiohttp.web.Request) -> aiohttp.web.Response:
		result = await self._controller.execute_command(todo.operations.tasks.DeleteTasks, request['payload'])
		response = aiohttp.web.HTTPNotFound(reason = json.dumps(
			{'error': f'Task with id = "{request["payload"]["id"]}" not found'}
		))
		if result:
			response = aiohttp.web.HTTPNoContent()
		return response


	async def _ping(self, request: aiohttp.web.Request) -> aiohttp.web.Response:
		return aiohttp.web.Response(text = 'pong')
