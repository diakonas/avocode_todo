import importlib
import json
import types
import os

import asyncpg
import aiohttp.test_utils
import aiohttp.web
import pytest

import todo.api
import todo.controller
import todo.middleware



@pytest.fixture
def settings() -> types.ModuleType:
	profile = os.environ.get('PROFILE', 'dev')
	_settings = importlib.import_module(f'settings.{profile}')
	setattr(_settings, 'PROFILE', profile)
	setattr(_settings, 'PREFIX', f"{_settings.APP_NAME}-{profile}")
	return _settings


@pytest.fixture(autouse = True)
async def db_connection(settings: types.ModuleType) -> asyncpg.Connection:
	connection = await asyncpg.connect(dsn = settings.DB_URI)
	await connection.execute(f'TRUNCATE TABLE task RESTART IDENTITY')
	yield connection


@pytest.fixture
async def web_server(db_connection: asyncpg.Connection) -> aiohttp.web.Application:
	controller = todo.controller.Controller(settings, db_connection)
	web_server = aiohttp.web.Application(middlewares = [
		todo.middleware.validation_handler(),
		todo.middleware.error_handler()
	])
	api = todo.api.API(settings, web_server.router, controller)
	api.register()
	return web_server


@pytest.fixture
async def test_client(web_server: aiohttp.web.Application):
	server = aiohttp.test_utils.TestServer(web_server)
	client = aiohttp.test_utils.TestClient(server)
	await client.start_server()
	yield client
	await client.close()



@pytest.mark.asyncio
async def test_create_tasks(test_client :aiohttp.test_utils.TestClient) -> None:
	# Given task data
	task_data = {'label': 'Task A'}

	# When I call 'POST /test' URL with the task data
	response = await test_client.post('/tasks', data = json.dumps(task_data))

	# Then I should response data with the created task and status code '201'
	response_data = json.loads(await response.json())
	assert response.status == 201
	assert response_data == {'task': {
		'label': 'Task A',
		'completed': False,
		'subtasks': [],
		'id': response_data['task']['id']
	}}

	# And give data with task and sub-tasks as well
	subtask_data = {'label': 'Task B', 'subtasks': [{'label': 'Sub B'}]}

	# When I call 'POST /test' URL with the sub-task data
	response = await test_client.post('/tasks', data = json.dumps(subtask_data))

	# Then I should see response data with the created task including sub-tasks and status code '201'
	response_data = json.loads(await response.json())
	assert response.status == 201
	assert response_data == {'task': {
		'label': 'Task B',
		'completed': False,
		'subtasks': [{
			'id': response_data['task']['subtasks'][0]['id'],
			'label': 'Sub B',
			'completed': False,
			'subtasks': []
		}],
		'id': response_data['task']['id']
	}}


@pytest.mark.asyncio
async def test_read_tasks(test_client: aiohttp.test_utils.TestClient) -> None:
	# Given task data
	task_A_data = {'label': 'Task A', 'subtasks': [{'label': 'Sub A'}]}
	task_B_data = {'label': 'Task B', 'subtasks': [{'label': 'Sub B'}]}

	# And two tasks each with one sub-task inserted in database
	await test_client.post('/tasks', data = json.dumps(task_A_data))
	await test_client.post('/tasks', data = json.dumps(task_B_data))

	# When I request 'GET /tasks'
	response = await test_client.get('/tasks')

	# Then I should get response data with all the tasks inserted and status code '200'
	response_data = await response.json()
	assert response.status == 200
	assert response_data == {'tasks': [{
		'label': 'Task A',
		'completed': False,
		'subtasks': [{
			'id': response_data['tasks'][0]['subtasks'][0]['id'],
			'label': 'Sub A',
			'completed': False,
			'subtasks': []
		}],
		'id': response_data['tasks'][0]['id']
	}, {
		'label': 'Task B',
		'completed': False,
		'subtasks': [{
			'id': response_data['tasks'][1]['subtasks'][0]['id'],
			'label': 'Sub B',
			'completed': False,
			'subtasks': []
		}],
		'id': response_data['tasks'][1]['id']
	}]}

	# And when I request the tasks with specific ID, i.e.: 'GET /tasks/:id'
	response = await test_client.get(f"/tasks/{response_data['tasks'][0]['id']}")

	# Then I should get response data with the task of the given ID with its sub-tasks and status code '200'
	response_data = await response.json()
	assert response.status == 200
	assert response_data == {'tasks': [{
		'label': 'Task A',
		'completed': False,
		'subtasks': [{
			'id': response_data['tasks'][0]['subtasks'][0]['id'],
			'label': 'Sub A',
			'completed': False,
			'subtasks': []
		}],
		'id': response_data['tasks'][0]['id']
	}]}


@pytest.mark.asyncio
async def test_update_tasks(test_client: aiohttp.test_utils.TestClient) -> None:
	# Given task data
	task_data = {'label': 'Task A', 'subtasks': [{'label': 'Sub A'}]}

	# And task with one sub-task inserted in database
	create_response = await test_client.post('/tasks', data = json.dumps(task_data))
	created_task = json.loads(await create_response.json())

	# When I call 'PUT /tasks/:id' together with "label" parameter to change
	response = await test_client.put(f"/tasks/{created_task['task']['id']}", data = json.dumps({'label': 'Task B'}))

	# Then I should get response data with the tasks changed and status code '200'
	response_data = await response.json()
	assert response.status == 200
	assert response_data == {'task': {
		'label': 'Task B',
		'completed': False,
		'subtasks': [{
			'id': response_data['task']['subtasks'][0]['id'],
			'label': 'Sub A',
			'completed': False,
			'subtasks': []
		}],
		'id': response_data['task']['id']
	}}

	# And when I call 'PUT /tasks/:id' together with "completed" parameter to change
	response = await test_client.put(f"/tasks/{created_task['task']['id']}", data = json.dumps({'completed': True}))

	# Then I should get response data with all tasks changed to "completed" and status code '200'
	response_data = await response.json()
	assert response.status == 200
	assert response_data == {'task': {
		'label': 'Task B',
		'completed': True,
		'subtasks': [{
			'id': response_data['task']['subtasks'][0]['id'],
			'label': 'Sub A',
			'completed': True,
			'subtasks': []
		}],
		'id': response_data['task']['id']
	}}


@pytest.mark.asyncio
async def test_delete_tasks(test_client: aiohttp.test_utils.TestClient) -> None:
	# Given task data
	task_data = {'label': 'Task A', 'subtasks': [{'label': 'Sub A'}]}

	# And task with one sub-task inserted in database
	create_response = await test_client.post('/tasks', data = json.dumps(task_data))
	created_task = json.loads(await create_response.json())

	# When I request 'DELETE /tasks/:id'
	response = await test_client.delete(f"/tasks/{created_task['task']['id']}")

	# Then I should get empty response with "No Content" and status code '204'
	response_data = await response.text()
	assert response.status == 204
	assert response_data == ''
