from typing import Any, Dict, List, Optional, Tuple
import uuid



class Task:


	def __init__(
		self,
		id: uuid.UUID,
		label: str,
		completed: bool = False,
		parent_id: Optional[uuid.UUID] = None,
		subtasks: Optional[List] = None
	) -> None:
		self.id = id
		self.label = label
		self.completed = completed
		self.parent_id = parent_id
		self.subtasks = subtasks


	def to_dict(self) -> Dict[str, Any]:
		return {
			'id': str(self.id),
			'label': self.label,
			'completed': self.completed,
			'subtasks': [task.to_dict() for task in self.subtasks]
		}



def task_tree(tasks: List[Tuple]) -> List[Task]:
	'''
	Takes linear list of tuples representing all tasks and transform it to list of "top-level" `Task`s
	where all sub-tasks are stored hierarchically in their parent `Task`s
	'''
	top_level = []
	task_map = {task[0]: Task(*task, subtasks = []) for task in tasks}
	for task in task_map.values():
		if task.parent_id:
			task_map[task.parent_id].subtasks.append(task)
		else:
			top_level.append(task)
	return top_level


def task_list(
	result: List[Tuple],
	task: Dict[str, Any],
	task_id: uuid.UUID,
	parent_id: Optional[uuid.UUID] = None
) -> List[Tuple]:
	'''
	Takes task dictionary, which may possibly contain additional nested sub-tasks, and transform it to a linear list
	of single tasks, where each sub-task refers to its parent-task via 'parent_id' property.
	'''
	result.append((task_id, task['label'], bool(task.get('completed', False)), parent_id))
	if task.get('subtasks'):
		for subtask in task['subtasks']:
			return task_list(result, subtask, uuid.uuid4(), task_id)
	return result


def flatten_task(task: Task, result: List[Task]) -> List:
	'''
	Takes `Task` and returns "flat" list of all its nested sub-tasks together with the task itself.
	'''
	if task not in result:
		result.append(task)
		for sub_task in task.subtasks:
			return flatten_task(sub_task, result)
	return result
