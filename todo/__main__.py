import asyncio
import importlib
import logging
import signal
import os
import os.path

import aiohttp.web
import asyncpg

import todo.api
import todo.controller
import todo.middleware



def main() -> None:
	# Import settings
	profile = os.environ.get('PROFILE', 'dev')
	settings = importlib.import_module(f'settings.{profile}')
	setattr(settings, 'PROFILE', profile)
	setattr(settings, 'PREFIX', f"{settings.APP_NAME}-{profile}")

	logger = logging.getLogger(settings.PREFIX)
	logging.basicConfig(level = settings.LOG_LEVEL)

	loop = asyncio.get_event_loop()

	# Application
	db_connection = loop.run_until_complete(asyncpg.connect(settings.DB_URI))
	controller = todo.controller.Controller(settings, db_connection)
	web_server = aiohttp.web.Application(middlewares = [
		todo.middleware.validation_handler(),
		todo.middleware.error_handler(),
	])
	api = todo.api.API(settings, web_server.router, controller)
	api.register()

	def on_shutdown():
		logger.info('Shutting down ...')
		loop.stop()

	loop.add_signal_handler(signal.SIGINT, on_shutdown)
	loop.add_signal_handler(signal.SIGTERM, on_shutdown)

	try:
		aiohttp.web.run_app(web_server, host = settings.WEB_SERVER_HOST, port = settings.WEB_SERVER_PORT)
	except KeyboardInterrupt:
		pass
	finally:
		on_shutdown()



if __name__ == '__main__':
	main()
