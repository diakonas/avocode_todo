import os.path

import decouple



APP_NAME = 'avocode-todo'


DB_URI = decouple.config('DB_URI', default = 'postgres://postgres@localhost:5432')


WEB_SERVER_HOST: str = decouple.config('WEB_SERVER_HOST', default = '0.0.0.0')
WEB_SERVER_PORT: int = decouple.config('WEB_SERVER_PORT', default = 8000)
