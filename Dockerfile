FROM python:3.7

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip install -r requirements.txt

COPY . /usr/src/app/

RUN chown -R nobody /usr/src/app/

RUN usermod --home /tmp nobody
USER nobody

ENV PYTHONPATH /usr/src/app

CMD ["python", "-m", "todo"]
